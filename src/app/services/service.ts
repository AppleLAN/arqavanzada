import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class Service {

  constructor(private http: Http) {
  }

  getWindData() {
    return this.http.get('http://localhost:8000/analog/history/wind');
  }

  getTemperatureData() {
    return this.http.get('http://localhost:8000/analog/history/temperature');
  }

  getHumidityData() {
    return this.http.get('http://localhost:8000/analog/history/humidity');
  }

  getWindSensor() {
    return this.http.get('http://localhost:8000/analog/parsed/wind');
  }

  getTemperatureSensor() {
    return this.http.get('http://localhost:8000/analog/parsed/temperature');
  }

  getHumiditySensor() {
    return this.http.get('http://localhost:8000/analog/parsed/humidity');
  }

  getFunction(type: string) {
    return this.http.get(`http://localhost:8000/function/${type}`);
  }

  writeFunction(type: string, a: number, b: number, c: number) {
    const param = {
      type,
      a,
      b,
      c
    };
    return this.http.post('http://localhost:8000/function', param);
  }

  setEmail(email: string) {
    const param = {
      email
    };
    return this.http.post('http://localhost:8000/email', param);
  }

  getEmail() {
    return this.http.get('http://localhost:8000/email');
  }

  setThreshold(type: string, start: number, end: number) {
    const param = {
      type,
      start,
      end
    };
    return this.http.post('http://localhost:8000/threshold', param);
  }

  getThreshold(type: string) {
    return this.http.get(`http://localhost:8000/threshold/${type}`);
  }

  getLightStatus() {
    return this.http.get(`http://localhost:8000/light`);
  }

  turnOnLight(start, end) {
    const param = {
      start,
      end
    };
    return this.http.post('http://localhost:8000/light', param);
  }

  turnOnLightDirectly() {
    return this.http.post('http://localhost:8000/digital/6/1', '');
  }

  turnOffLightDirectly() {
    return this.http.post('http://localhost:8000/digital/6/0', '');
  }

  getLightStatusDirectly() {
    return this.http.get('http://localhost:8000/digital/6');
  }

}
