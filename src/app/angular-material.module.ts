import { NgModule } from '@angular/core';
// Angular Materials 2
import {
  MatInputModule, MatButtonModule, MatCardModule, MatSnackBarModule,
  MatMenuModule, MatDialogModule, MatRadioModule, MatIconRegistry, MatIconModule,
  MatExpansionModule, MatSlideToggleModule, MatSelectModule, MatAutocompleteModule,
  MatListModule, MatCheckboxModule, MatTabsModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';

const materialModules = [
  MatInputModule,
  MatButtonModule,
  MatCardModule,
  MatMenuModule,
  MatRadioModule,
  MatSnackBarModule,
  MatDialogModule,
  MatIconModule,
  MatExpansionModule,
  BrowserAnimationsModule,
  FlexLayoutModule,
  MatSlideToggleModule,
  MatSelectModule,
  MatAutocompleteModule,
  MatListModule,
  MatCheckboxModule,
  MatTabsModule
];

@NgModule({
  imports: materialModules,
  exports: materialModules
})
export class AngularMaterialModule {
  constructor(iconRegistry: MatIconRegistry) {
    iconRegistry.setDefaultFontSetClass('zmdi');
  }
}
