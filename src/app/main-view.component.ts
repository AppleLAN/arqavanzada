import { Http } from '@angular/http';
import { Service } from './services/service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./app.component.scss']
})
export class MainViewComponent implements OnInit {
  tiempoEntreMuestras: number;
  windChartData: any;
  windChartLabels: any;
  windType = 'wind';
  temperatureChartData: any;
  temperatureChartLabels: any;
  temperatureType = 'temperature';
  humidityChartData: any;
  humidityChartLabels: any;
  humidityType = 'humidity';
  windSensorValue: any;
  tempetureSensorValue: any;
  humiditySensorValue: any;
  isAdmin: boolean;

  constructor(private router: Router, private s: Service, private http: Http) {}

  ngOnInit() {
    this.tiempoEntreMuestras = !!localStorage.getItem('time') ? Number(localStorage.getItem('time')) * 1000 : 15000 ;
    this.isAdmin = localStorage.getItem('currentUser') === 'admin' ? true : false;
    this.callEndpoints();
    setInterval(() => {
      this.callEndpoints();
    }, this.tiempoEntreMuestras);
  }

  callEndpoints() {
    this.s.getWindData().subscribe(data => {
      this.windChartData = null;
      // Read the result field from the JSON response.
      this.windChartData = [{data: data.json().map(d => d.value).reverse(), label: 'Viento (km/h)'}];
      this.windChartLabels = data.json().map(d => d.time).reverse();
    });

    this.s.getTemperatureData().subscribe(data => {
      this.temperatureChartData = null;
      // Read the result field from the JSON response.
      this.temperatureChartData = [{data: data.json().map(d => d.value).reverse(), label: 'Temperatura (C)'}];
      this.temperatureChartLabels = data.json().map(d => d.time).reverse();
    });

    this.s.getHumidityData().subscribe(data => {
      this.humidityChartData = null;
      // Read the result field from the JSON response.
      this.humidityChartData = [{data: data.json().map(d => d.value).reverse(), label: 'Humedad (%)'}];
      this.humidityChartLabels = data.json().map(d => d.time).reverse();
    });

    this.s.getWindSensor().subscribe(data => {
      this.windSensorValue = data.json();
    });

    this.s.getTemperatureSensor().subscribe(data => {
      this.tempetureSensorValue = data.json();
    });

    this.s.getHumiditySensor().subscribe(data => {
      this.humiditySensorValue = data.json();
    });
  }

  logOut() {
    localStorage.setItem('currentUser', 'none');
    this.router.navigate(['/login']);
  }

  configPanel() {
    this.router.navigate(['/admin']);
  }
}
