import { Service } from './services/service';
import { AuthGuard } from './auth.guard';
import { LoginComponent } from './login.component';
import { MainViewComponent } from './main-view.component';
import { AdminComponent } from './admin/admin.component';
import { AngularMaterialModule } from './angular-material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { ChartComponent } from './chart';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { AppComponent } from './app.component';
import { NgxGaugeModule } from 'ngx-gauge';
import {CsvService} from './services/csv';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'main',
    pathMatch: 'full'
  }, {
    path: 'main',
    component: MainViewComponent
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthGuard]
  }, {
    path: 'login',
    component: LoginComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    MainViewComponent,
    AdminComponent,
    LoginComponent,
    ChartComponent
  ],
  imports: [
    ChartsModule,
    RouterModule.forRoot(
      appRoutes
    ),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    NgxGaugeModule,
    AngularMaterialModule
  ],
  providers: [AuthGuard, Service, CsvService],
  bootstrap: [AppComponent]
})
export class AppModule { }
