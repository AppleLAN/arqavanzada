import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { Service } from '../services/service';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';
import {CsvService} from '../services/csv';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html'
})

export class AdminComponent implements OnInit {
  tiempoEntreMuestras: number;
  umbralInferiorViento;
  umbralSuperiorViento;
  umbralInferiorTemperatura;
  umbralSuperiorTemperatura;
  umbralInferiorHumedad;
  umbralSuperiorHumedad;
  email: string;
  horarioLuces: number;
  funcionDeViento: any;
  funcionDeTemperatura: any;
  funcionDeHumedad: any;
  valorAFuncionViento: number;
  valorBFuncionViento: number;
  valorCFuncionViento: number;
  valorAFuncionTemperatura: number;
  valorBFuncionTemperatura: number;
  valorCFuncionTemperatura: number;
  valorAFuncionHumedad: number;
  valorBFuncionHumedad: number;
  valorCFuncionHumedad: number;
  horaLuzEncendido: number;
  minutosLuzEncendido: number;
  segundosLuzEncendido: number;
  horaLuzApagado: number;
  minutosLuzApagado: number;
  registroStart: number;
  registroEnd: number;

  constructor(private router: Router, private sv: Service, private csvService: CsvService) {
    this.umbralInferiorViento = 0;
    this.umbralSuperiorViento = 0;
    this.umbralInferiorTemperatura = 0;
    this.umbralSuperiorTemperatura = 0;
    this.umbralInferiorHumedad = 0;
    this.umbralSuperiorHumedad = 0;
    this.email = null;
    this.horarioLuces = 12543;
  }

  ngOnInit() {
    this.tiempoEntreMuestras = !!localStorage.getItem('time') ? Number(localStorage.getItem('time')) : 15;
    this.sv.getEmail().subscribe((data) => {
      this.email = data.json().email;
    });

    this.sv.getThreshold('wind').subscribe((data) => {
      if (data) {
        this.umbralInferiorViento = data.json().start;
        this.umbralSuperiorViento = data.json().end;
      }
    });

    this.sv.getThreshold('temperature').subscribe((data) => {
      if (data) {
        this.umbralInferiorTemperatura = data.json().start;
        this.umbralSuperiorTemperatura = data.json().end;
      }
    });

    this.sv.getThreshold('humidity').subscribe((data) => {
      if (data) {
        this.umbralInferiorHumedad = data.json().start;
        this.umbralSuperiorHumedad = data.json().end;
      }
    });

    this.sv.getFunction('wind').subscribe((data) => {
      this.funcionDeViento = data.json();
    });

    this.sv.getFunction('temperature').subscribe((data) => {
      this.funcionDeTemperatura = data.json();
    });

    this.sv.getFunction('humidity').subscribe((data) => {
      this.funcionDeHumedad = data.json();
    });
  }

  logOut() {
    localStorage.setItem('currentUser', 'none');
    this.router.navigate(['/login']);
  }

  mainView() {
    this.router.navigate(['/main']);
  }

  assignEmail() {
    this.sv.setEmail(this.email).subscribe();
  }

  changeTime() {
    localStorage.setItem('time', `${this.tiempoEntreMuestras}`);
  }

  changeWindFunction() {
    this.sv.writeFunction('wind', this.valorAFuncionViento, this.valorBFuncionViento, this.valorCFuncionViento).subscribe();
  }

  changeTemperatureFunction() {
    this.sv.writeFunction('temperature', this.valorAFuncionTemperatura, this.valorBFuncionTemperatura, this.valorCFuncionTemperatura)
      .subscribe();
  }

  changeHumidityFunction() {
    this.sv.writeFunction('humidity', this.valorAFuncionHumedad, this.valorBFuncionHumedad, this.valorCFuncionHumedad).subscribe();
  }

  changeWindThreshold() {
    this.sv.setThreshold('wind', this.umbralInferiorViento, this.umbralSuperiorViento).subscribe();
  }

  changeTemperatureThreshold() {
    this.sv.setThreshold('temperature', this.umbralInferiorTemperatura, this.umbralSuperiorTemperatura).subscribe();
  }

  changeHumidityThreshold() {
    this.sv.setThreshold('humidity', this.umbralInferiorHumedad, this.umbralSuperiorHumedad).subscribe();
  }

  turnOnLight() {
    this.sv.turnOnLightDirectly().subscribe();
  }

  turnOffLight() {
    this.sv.turnOffLightDirectly().subscribe();
  }

  changeLightTimes() {
    const start = {
      hour: this.horaLuzEncendido,
      minutes: this.minutosLuzEncendido,
      seconds: 0
    };
    const end = {
      hour: this.horaLuzApagado,
      minutes: this.minutosLuzApagado,
      seconds: 0
    };
    this.sv.turnOnLight(start, end).subscribe();
  }

  downloadRegistry(type: string) {
      switch (type) {
        case 'wind':
          this.sv.getWindData().subscribe(data => {
            let dataToSend = data.json();
            if (this.registroStart && this.registroEnd) {
              const registryData = data.json().filter(elem => {
                const n = elem.time.indexOf('.');
                elem.time = elem.time.substring(0, n !== -1 ? n : elem.time.length);
                const date = new Date(elem.time);
                const hours = date.getHours();
                return this.registroStart < hours && this.registroEnd > hours;
              });
              dataToSend = registryData;
            }
            this.csvService.download(dataToSend, 'Viento');
          });
          break;
        case 'temperature':
          this.sv.getTemperatureData().subscribe(data => {
            let dataToSend = data.json();
            if (this.registroStart && this.registroEnd) {
              const registryData = data.json().filter(elem => {
                const n = elem.time.indexOf('.');
                elem.time = elem.time.substring(0, n !== -1 ? n : elem.time.length);
                const date = new Date(elem.time);
                const hours = date.getHours();
                return this.registroStart < hours && this.registroEnd > hours;
              });
              dataToSend = registryData;
            }
            this.csvService.download(dataToSend, 'Temperatura');
          });
          break;
        case 'humidity':
          this.sv.getHumidityData().subscribe(data => {
            let dataToSend = data.json();
            if (this.registroStart && this.registroEnd) {
              const registryData = data.json().filter(elem => {
                const n = elem.time.indexOf('.');
                elem.time = elem.time.substring(0, n !== -1 ? n : elem.time.length);
                const date = new Date(elem.time);
                const hours = date.getHours();
                return this.registroStart < hours && this.registroEnd > hours;
              });
              dataToSend = registryData;
            }
            this.csvService.download(dataToSend, 'Humedad');
          });
          break;
      }
  }

}
