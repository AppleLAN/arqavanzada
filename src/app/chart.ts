import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.html'
})
export class ChartComponent implements OnInit {
  lineChartData: Array<any>;
  lineChartLabels: Array<any>;
  lineChartOptions: any;
  lineChartColors: Array<any>;
  lineChartLegend: Boolean;

  @Input() chartType: String = 'line';
  @Input() demo: Boolean = false;
  @Input() data: any;
  @Input() labels: any;
  @Input() color = 'wind';
  @Input() sensorData;

  constructor() {}
  ngOnInit() {
    if (this.data && this.labels) {
      this.lineChartData = this.data;
      this.lineChartLabels = this.labels.map(date => {
        const n = date.indexOf('.');
        const dateString = date.substring(0, n !== -1 ? n : date.length);
        const endDate = new Date(dateString);
        const day = endDate.getDate();
        const monthIndex = endDate.getMonth();
        const year = endDate.getFullYear();
        const hours = endDate.getHours();
        const minutes = endDate.getMinutes();
        return day + '/' + monthIndex + '/' + year + ' - ' + hours + ': ' + minutes;
      });
    } else {
      this.lineChartData = [
        {data: [65, -20, 80], label: 'Sensores'}
      ];
      this.lineChartLabels = ['Viento (km/h)', 'Temperatura (C)', 'Humedad (%)'];
    }
    this.lineChartOptions = {
      responsive: false,
      maintainAspectRatio: false,
      scales : {
        yAxes: [{
           ticks: {
              max : !!this.sensorData ? this.sensorData.max : 1023,
              min : !!this.sensorData ? this.sensorData.min : 0
            }
        }]
      }
    };
    switch (this.color) {
      case 'wind':
        this.lineChartColors = [
          { // grey
            backgroundColor: 'rgba(255,165,0,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
          },
          { // dark grey
            backgroundColor: 'rgba(77,83,96,0.2)',
            borderColor: 'rgba(77,83,96,1)',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
          },
          { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
          }
        ];
        break;
      case 'temperature':
        this.lineChartColors = [
          { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
          },
          { // dark grey
            backgroundColor: 'rgba(77,83,96,0.2)',
            borderColor: 'rgba(77,83,96,1)',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
          },
          { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
          }
        ];
        break;
      case 'humidity':
        this.lineChartColors = [
          { // grey
            backgroundColor: 'rgba(255,105,180,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
          },
          { // dark grey
            backgroundColor: 'rgba(77,83,96,0.2)',
            borderColor: 'rgba(77,83,96,1)',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
          },
          { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
          }
        ];
      break;
      default:
        break;
    }
    this.lineChartLegend = true;
  }

  // events
  chartClicked(e: any): void {
    console.log(e);
  }

  chartHovered(e: any): void {
    console.log(e);
  }
}
