import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})

export class LoginComponent {
  loginForm: FormGroup;

  constructor(private fb: FormBuilder, private router: Router) {
    this.loginForm = this.fb.group({
      user: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  submit({ value }: { value: any }) {
    if (value.user === 'admin' && value.password === 'secret') {
      localStorage.setItem('currentUser', 'admin');
    }
    this.router.navigate(['/main']);
  }
}
